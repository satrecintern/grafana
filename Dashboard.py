import requests
import json
import Folder as folder

# 기능 수행 후 결과 출력
def result_print(status,response,Requirement):
    type=Requirement['type']
    folder_name = Requirement['folder']
    dashboard_name = Requirement.get('dashboard_name')
    panel_name = Requirement.get('panel_name')
    values = Requirement.get('values')

    if status=='Failed': # 결과가 fail일 때
        print('Failed')
        print('- type : ', type)
        print('- folder : ', folder_name)
        print('- dashboard_name : ', dashboard_name)
        print('- panel_name : ', panel_name)
        print('- values : ', values)
        print('- 이유 : ', response,'\n')
    elif status=='Success':# 결과가 success일 때
        print('Success')
        print('- type : ', type)
        print('- folder : ', folder_name)
        print('- dashboard_name : ', dashboard_name)
        print('- panel_name : ', panel_name)
        print('- values : ', values)
        print('- 응답 정보 : ', response, '\n')
    else:# 결과를 모를 때(API의 요청에 대한 response 필요)
        if response.status_code==200:
            print('Success')
        else:
            print('Failed')
        print('- type : ', type)
        print('- folder : ', folder_name)
        print('- dashboard_name : ', dashboard_name)
        print('- panel_name : ', panel_name)
        print('- values : ', values)
        print('- 응답 정보 : ', response.json(), '\n')

# Dashboard 업데이트
def send_data(new_dash_data):
    server = 'http://localhost:3000'
    url = server + '/api/dashboards/db'
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer eyJrIjoiTTY4VVdGQzYxd3BnWFo3SnBBcWFob2EyVmhhYjBhSnAiLCJuIjoiYWRtaW4iLCJpZCI6MX0="
    }

    r = requests.post(url=url, headers=headers, data=json.dumps(new_dash_data))
    return r

# Dashboard 정보 가져오기
def get_dashboard(uid):
    server = 'http://localhost:3000'
    url = server + '/api/dashboards/uid/'+uid
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer eyJrIjoiTTY4VVdGQzYxd3BnWFo3SnBBcWFob2EyVmhhYjBhSnAiLCJuIjoiYWRtaW4iLCJpZCI6MX0="
    }

    r = requests.get(url=url, headers=headers)
    return r.json()

#Dashboard의 uid 가져오기
def get_dashboard_uid(folder_id,dash_title):
    server = 'http://localhost:3000'
    url = server + '/api/search?folderIds='+str(folder_id)+'&query='+dash_title
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer eyJrIjoiTTY4VVdGQzYxd3BnWFo3SnBBcWFob2EyVmhhYjBhSnAiLCJuIjoiYWRtaW4iLCJpZCI6MX0="
    }

    r = requests.get(url=url, headers=headers)

    if not r.json():
        uid=0
    else:
        uid=r.json()[0]['uid']
    return uid

######## Dashboard 생성 ########
def create_dashboard(folderId,dashboard):
    dashboard['folderId']=folderId
    response=send_data(dashboard)
    return response

def multi_create_dashboard(Requirement):
    folderId=folder.get_folder_id(Requirement['folder'])
    dashboard_list=Requirement['dashboard_name']
    responses=[]
    result_count=0
    
    # 입력 list의 모든 dashboard 생성
    for dashboard in dashboard_list:
        response=create_dashboard(folderId,dashboard) # Dashboard 생성
        if response.status_code==200:
            result_count+=1
        responses.append(response.json())

    if result_count==len(dashboard_list):# 모든 Dashboard가 생성됐을 때
        result_print('Success',responses,Requirement)
    else:
        result_print('Failed','모든 대시 보드가 생성되지 않았습니다', Requirement)

######## Dashboard 삭제 ########
def delete_dashboard(uid):
    server = 'http://localhost:3000'
    url = server + '/api/dashboards/uid/'+uid
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer eyJrIjoiTTY4VVdGQzYxd3BnWFo3SnBBcWFob2EyVmhhYjBhSnAiLCJuIjoiYWRtaW4iLCJpZCI6MX0="
    }
    r = requests.delete(url=url, headers=headers)
    return r

def multi_delete_dashboard(Requirement):
    folderId = folder.get_folder_id(Requirement['folder'])
    dashboard_list = Requirement['dashboard_name']
    responses = []
    result_count = 0

    # 입력 list의 모든 dashboard 삭제
    for dashboard in dashboard_list:
        uid = get_dashboard_uid(folderId, dashboard)
        if uid != 0:
            response=delete_dashboard(uid) # Dashboard 삭제
            if response.status_code == 200:
                result_count += 1
            responses.append(response.json())
        else:
            result_print('Failed', '해당 대시 보드가 없습니다.', Requirement)

    if result_count == len(dashboard_list): # 모든 Dashboard가 삭제됐을 때
        result_print('Success', responses, Requirement)
    else:
        result_print('Failed', '모든 대시 보드가 삭제되지 않았습니다', Requirement)

