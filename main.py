import yaml
import Folder as folder
import Dashboard as dashboard
import Panel as panel
import Alert as alert
import Revise as revise

if __name__ == '__main__':
    with open('option.yml') as f:
        option = yaml.load(f, Loader=yaml.SafeLoader)

    Requirements=option['Recipe']
    for recipe in Requirements:
        type=recipe['type']

        if type=='create_dashboard':
            dashboard.multi_create_dashboard(recipe)
        elif type=='delete_dashboard':
            dashboard.multi_delete_dashboard(recipe)
        elif type=='create_panel':
            panel.multi_create_panel(recipe)
        elif type=='delete_panel':
            panel.multi_delete_panel(recipe)
        elif type=='create_alert':
            alert.multi_create_alert(recipe)
        elif type=='delete_alert':
            alert.multi_delete_alert(recipe)
        elif type=='revise':
            revise.multi_revise(recipe)
