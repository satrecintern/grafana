import copy
import Panel as panel
import Dashboard as dashboard
import Folder as folder

######## 알림 생성 #########
def create_alert(uid,panel_name,alerts):
    dash_data=dashboard.get_dashboard(uid)
    new_dash_data = copy.deepcopy(dash_data)
    folderId = dash_data['meta']['folderId']

    panel_data = new_dash_data['dashboard']['panels']
    panel_index = panel.get_panel_index(panel_data, panel_name)

    if panel_index == -1:
        return 'Failed','해당 panel을 찾을 수 없습니다'
    else:
        # 입력 list의 모든 알림 생성
        for alert in alerts:
            if "alert" in new_dash_data['dashboard']['panels'][panel_index]: # 해당 panel에 알림이 존재할 때
                if 'conditions' in alert: # 추가 할 알림이 초기 Alert설정일 때(condition만 있는것이 아니라 여러 옵션이 존재할 때)
                    return 'Failed', '이미 설정된 알림이 있습니다. condition만 입력해주세요'
                else: # Alert condition 추가
                    new_dash_data['dashboard']['panels'][panel_index]['alert']['conditions'].append(alert)
            else: # 해당 panel에 알림이 없을 때
                if 'conditions' in alert: # 추가 할 알림이 초기 Alert설정일 때 알림 추가
                    new_dash_data['dashboard']['panels'][panel_index]['alert']=alert
                else:
                    return 'Failed','초기 alert가 없습니다. 먼저 초기 alert를 설정해주세요'
        new_dash_data['folderId'] = folderId
        response=dashboard.send_data(new_dash_data) # Dashboard 업데이트
        return 'unknown',response

def multi_create_alert(Requirement):
    folderId = folder.get_folder_id(Requirement['folder'])
    dashboard_name = Requirement['dashboard_name']
    panel_name = Requirement['panel_name']
    alerts=Requirement['values']
    uid = dashboard.get_dashboard_uid(folderId, dashboard_name)

    if uid != 0:
        status,response=create_alert(uid, panel_name,alerts) # 모든 알림 생성
        dashboard.result_print(status,response, Requirement)
    else:
        dashboard.result_print('Failed','해당 대시 보드가 없습니다.',Requirement)

######## 알림 삭제 #########
def delete_alert(uid,panel_name,alerts):
    dash_data = dashboard.get_dashboard(uid)
    new_dash_data = copy.deepcopy(dash_data)
    folderId = dash_data['meta']['folderId']
    alerts.sort(reverse=True)

    panel_data = new_dash_data['dashboard']['panels']
    panel_index = panel.get_panel_index(panel_data, panel_name)

    if panel_index == -1:
        return 'Failed','해당 panel을 찾을 수 없습니다'
    else:
        if "alert" in new_dash_data['dashboard']['panels'][panel_index] and len(new_dash_data['dashboard']['panels'][panel_index]['alert']['conditions'])>0:
            for alert_index in alerts:
                try:
                    if len(new_dash_data['dashboard']['panels'][panel_index]['alert']['conditions'])>1:
                        del new_dash_data['dashboard']['panels'][panel_index]['alert']['conditions'][alert_index]
                    else:
                        del new_dash_data['dashboard']['panels'][panel_index]['alert']
                except:
                    return 'Failed', '알림의 index가 알맞지 않습니다 '
            new_dash_data['folderId'] = folderId
            response=dashboard.send_data(new_dash_data)
            return 'unknown',response
        else:
            return 'Failed','해당 panel에 이미 알림이 없습니다!'

def multi_delete_alert(Requirement):
    folderId = folder.get_folder_id(Requirement['folder'])
    dashboard_name = Requirement['dashboard_name']
    panel_name = Requirement['panel_name']
    alerts = Requirement['values']
    uid = dashboard.get_dashboard_uid(folderId, dashboard_name)

    if uid != 0:
        status, response=delete_alert(uid, panel_name, alerts) # 모든 알림 삭제
        dashboard.result_print(status, response, Requirement)
    else:
        dashboard.result_print('Failed', '해당 대시 보드가 없습니다.', Requirement)




