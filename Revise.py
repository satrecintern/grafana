import Folder as folder
import Dashboard as dashboard
import Panel as panel
import copy

######## Dashboard or Panel 수정 ########
# 수정 할 target의 마지막 위치를 반환하는 메소드
def return_final_position(target,final_position,target_len,count):
    if count==target_len-1:
        return final_position
    else:
        if target[count].isdigit():
            value=int(target[count])
        else:
            value=target[count]
        return return_final_position(target,final_position[value],target_len,count+1)

def revise(uid,values,panel_name):
    # Dashboard의 정보를 가져와서 copy
    dash_data = dashboard.get_dashboard(uid)
    new_dash_data = copy.deepcopy(dash_data)
    folderId = dash_data['meta']['folderId']

    # 입력 list의 Panel의 index 찾기
    panel_data = new_dash_data['dashboard']['panels']
    panel_index = panel.get_panel_index(panel_data, panel_name)

    if panel_name == None: # 입력 list에 Panel정보가 없을 때 -> Dashboard 수정
        Top_target=new_dash_data['dashboard']
    else: # 입력 list에 Panel정보가 있을 때 -> Panel 수정
        if panel_index == -1: # 입력 list의 Panel이 존재하지 않을 때
            return 'Failed', '해당 panel을 찾을 수 없습니다'
        else: # 입력 list의 Panel이 존재할 때
            Top_target = new_dash_data['dashboard']['panels'][panel_index]

    # 입력 list의 모든 value들을 수정
    for revise_target in values.keys():
        revise_value = values[revise_target]
        revise_target = revise_target.split('->')
        try:
            final_position = return_final_position(revise_target, Top_target, len(revise_target), 0)
            final_position[revise_target[len(revise_target) - 1]] = revise_value
        except:
            return 'Failed', '수정 할 target의 위치가 알맞지 않습니다'
    new_dash_data['folderId'] = folderId
    response=dashboard.send_data(new_dash_data) # 수정 된 Dashboard 업데이트
    return 'unknown',response


def multi_revise(Requirement):
    folderId = folder.get_folder_id(Requirement['folder'])
    dashboard_name = Requirement['dashboard_name']
    panel_name = Requirement.get('panel_name')
    values=Requirement['values']
    uid = dashboard.get_dashboard_uid(folderId, dashboard_name)

    if uid != 0: # 입력 list의 Dashboard가 있을 때
        status,response=revise(uid,values,panel_name) # Dashboard 수정
        dashboard.result_print(status, response, Requirement)
    else:
        dashboard.result_print('Failed', '해당 대시 보드가 없습니다.', Requirement)