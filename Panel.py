import requests
import json
import copy
import Dashboard as dashboard
import Folder as folder

# panel의 index 가져오기
def get_panel_index(panel_data,panel_title):
    panel_index=-1

    for i,panel in enumerate(panel_data):
        if panel['title']==panel_title:
            panel_index=i
    return panel_index

######## Panel 생성 #########git s
def create_panel(uid,panel_list):
    dash_data=dashboard.get_dashboard(uid)
    new_dash_data = copy.deepcopy(dash_data)

    folderId=dash_data['meta']['folderId']

    #panel id 설정
    if len(dash_data['dashboard']['panels'])==0:
        panel_id=2
    else:
        panel_id=dash_data['dashboard']['panels'][0]['id']+2

    # 입력 list의 모든 panel 생성
    for panel in panel_list:
        panel['id'] = panel_id
        new_dash_data['dashboard']['panels'].insert(0, panel)  # 새로운 panel 추가
        panel_id += 2

    new_dash_data['folderId'] = folderId
    response=dashboard.send_data(new_dash_data) # Dashboard 업데이트

    return response

def multi_create_panel(Requirement):
    folderId = folder.get_folder_id(Requirement['folder'])
    dashboard_name = Requirement['dashboard_name']
    panel_list=Requirement['panel_name']
    uid = dashboard.get_dashboard_uid(folderId, dashboard_name)

    if uid != 0:
        response=create_panel(uid, panel_list) # Panels 생성
        dashboard.result_print('unknown',response,Requirement)
    else:
        dashboard.result_print('Failed','해당 대시 보드가 없습니다.',Requirement)

######## Panel 삭제 #########
def delete_panel(uid,panel_list):
    dash_data = dashboard.get_dashboard(uid)
    new_dash_data = copy.deepcopy(dash_data)
    folderId = dash_data['meta']['folderId']

    # 입력 list의 모든 panel 삭제
    for panel_name in panel_list:
        panel_data = new_dash_data['dashboard']['panels']
        panel_index=get_panel_index(panel_data,panel_name) # 해당 panel의 index 찾기
        if panel_index==-1:
            return 'Failed','해당 panel을 찾을 수 없습니다'
        else:
            del new_dash_data['dashboard']['panels'][panel_index]
    new_dash_data['folderId'] = folderId

    response=dashboard.send_data(new_dash_data)
    return 'unknown',response

def multi_delete_panel(Requirement):
    folderId = folder.get_folder_id(Requirement['folder'])
    dashboard_name = Requirement['dashboard_name']
    panel_list = Requirement['panel_name']
    uid = dashboard.get_dashboard_uid(folderId, dashboard_name)

    if uid != 0:
        status, response=delete_panel(uid, panel_list) # Panels 삭제
        dashboard.result_print(status, response, Requirement)
    else:
        dashboard.result_print('Failed', '해당 대시 보드가 없습니다.', Requirement)




