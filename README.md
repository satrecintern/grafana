# 목적
Grafana의 JSON Model 기능은 수정을 쉽게 하고 유지 보수를 편리하게 하기 위한 기능인데 JSON 내의 정보 양이 많으면 서버가 꺼지거나 너무 느려서 제대로 활용하지 못한다. 이를 보완하여 관리의 효율성을 높이고자 한다.
또한 생성, 삭제, 수정과 같은 반복작업을 정해진 입력 list형식을 사용하여 자동화하여 편리성을 높이고자 한다.

# 사용 기술
+ Python requests
+ Grafana HTTP API

# 파일
#### 1. option.yml (입력 list 파일)
```
    type: delete_panel
    folder: create folder1
    dashboard_name: Test folder2
    panel_name:
      - Alert1
      - Alert3
```
- type : 수행하고자 하는 기능의 종류
- folder : 수행하고자 하는 대상의 폴더 이름
- dashboard_name : 수행하고자 하는 대상의 대시 보드 이름 or 대시 보드 객체
- panel_name : 수행하고자 하는 대상의 패널 이름 or 패널 객체 (생략 가능)
- values : 수행하고자 하는 대상의 알림 이름 or 알림 객체 or 수정값 (생략 가능)

위와 같은 형태가 하나의 기능을 수행하는 최소 단위의 입력으로 이러한 입력 list를 여러개의 배열로 추가하여 한번에 여러개의 기능을 수행

#### 2. main.py

입력 list인 option.yml을 읽어 각 type에 맞는 기능을 실행해주는 파일

#### 3. Dashboard.py

대시 보드와 관련된 기능 또는 전체적으로 영향을 주는 기능을 수행하는 파일

[전체적으로 영향을 주는 기능]

+ result_print(status,response,Requirement) : 해당 type에 맞는 기능을 수행 후 결과를 출력
+ send_data(new_dash_data): 대시 보드를 업데이트 하거나 JSON객체를 전달하여 생성하는 메소드
+ get_dashboard(uid) : uid에 해당하는 대시 보드의 정보를 가져오는 메소드
+ get_dashboard_uid(folder_id,dash_title) : 폴더ID와 대시 보드 이름을 이용하여 대시 보드의 uid를 가져오는 메소드

[대시 보드와 관련된 기능]

+ 대시 보드 생성
    + multi_create_dashboard(Requirement)
    + create_dashboard(folderId,dashboard)
    
+ 대시 보드 삭제
    + multi_delete_dashboard(Requirement)
    + delete_dashboard(uid)
    
#### 4. Panel.py

패널과 관련된 기능을 수행하는 파일

+ get_panel_index(panel_data,panel_title) : 찾고자 하는 패널의 데이터와 패널의 이름으로 몇번째의 index인지 찾아서 리턴하는 메소드
+ 패널 생성
    + multi_create_panel(Requirement)
    + create_panel(uid,panel_list)
    
+ 패널 삭제
    + multi_delete_panel(Requirement)
    + delete_panel(uid,panel_list)
    
#### 5. Alert.py

알림 설정과 관련된 기능을 수행하는 파일

+ 알림 생성
    + multi_create_alert(Requirement)
    + create_alert(uid,panel_name,alerts)
    
+ 알림 삭제
    + multi_delete_alert(Requirement)
    + delete_alert(uid,panel_name,alerts)
    
#### 6. Revise.py

대시 보드 수정 or 패널 수정과 관련된 기능을 수행하는 파일

+ return_final_position(target,final_position,target_len,count) : 수정 할 target의 마지막 위치를 반환하는 메소드
+ multi_revise(Requirement)
+ revise(uid,values,panel_name)

#### 7. Folder.py

폴더와 관련된 기능을 수행하는 파일

+ get_folder_id(folder_title) : 폴더 이름을 이용하여 해당 폴더의 Id를 리턴하는 메소드

# 실행
1. option.yml에 수행하고자 하는 기능에 맞춰 입력 list작성
2. main.py 실행