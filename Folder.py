import requests

# folder의 id 찾기
def get_folder_id(folder_title):
    if folder_title == 'General':
        folder_id = 0
    else:
        server = 'http://localhost:3000'
        url = server + '/api/search?query='+folder_title+'&type=dash-folder'
        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": "Bearer eyJrIjoiTTY4VVdGQzYxd3BnWFo3SnBBcWFob2EyVmhhYjBhSnAiLCJuIjoiYWRtaW4iLCJpZCI6MX0="
        }

        r = requests.get(url=url, headers=headers)
        folder_id=r.json()[0]['id']
    return folder_id

def get_all_folder():
    server = 'http://localhost:3000'
    url = server + '/api/folders?limit=10'
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer eyJrIjoiTTY4VVdGQzYxd3BnWFo3SnBBcWFob2EyVmhhYjBhSnAiLCJuIjoiYWRtaW4iLCJpZCI6MX0="
    }

    r = requests.get(url=url, headers=headers)
    print(r.json())

def get_folder(uid):
    server = 'http://localhost:3000'
    url = server + '/api/folders/'+uid
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer eyJrIjoiTTY4VVdGQzYxd3BnWFo3SnBBcWFob2EyVmhhYjBhSnAiLCJuIjoiYWRtaW4iLCJpZCI6MX0="
    }


    r = requests.get(url=url, headers=headers)
    print(r.json())

def create_folder():
    server = 'http://localhost:3000'
    url = server + '/api/folders'
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer eyJrIjoiTTY4VVdGQzYxd3BnWFo3SnBBcWFob2EyVmhhYjBhSnAiLCJuIjoiYWRtaW4iLCJpZCI6MX0="
    }
    data='''{
            "uid": null,
            "title": "create folder1"
        }'''

    r = requests.post(url=url, headers=headers, data=data)
    print(r.json())



